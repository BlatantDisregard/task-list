const React = require('react');
const ReactRedux = require('react-redux');
const _ = require('lodash');

const actionCreators = require('../state/reducer');

/**
 * The Task component renders a view for a single task.
 */

const Task = (props) => {
const onRemoveButtonClick = () => {
    props.onRemove(props.id);
};
return (
  <li>
  {props.description}
  <button onClick={onRemoveButtonClick}>&times;</button>
  </li>
);
};

/**
 * The TaskList component renders a view for a list
 * of tasks.
 */
const TaskList = React.createClass({
  // Display name (useful for debugging)
  displayName: 'TaskList',

  getInitialState: function() {
       return {newTaskDescription: 'wtvsd'};
  },

  createTaskComponent: function(task) {
    // Note: {...task} is a shortcut for setting properties with
    // names that match keys in the object. The following line is
    // equivalent to:
    //    <Task key={task.id} id={task.id} description={task.description}
    //      completed={task.completed} />
   return <Task key={task.id} onRemove={this.props.removeTask} {...task} />;
  },

  // Describe how to render the component
  render: function() {
    console.log('newTaskDescription="' + this.state.newTaskDescription + '"');

    const onDescriptionInputChange = (event) => {
       const newTaskDescription = event.target.value;
       this.setState({ newTaskDescription });
    };

    const onInsertButtonClick = () => {
      this.props.insertTask(this.state.newTaskDescription);
      const newTaskDescription = "";
      this.setState({ newTaskDescription });
    };

    let buttonDisabled = false;
    if(this.state.newTaskDescription == "") {
      buttonDisabled = true;
    }

    return (
      <div>
        <input type="text" value={this.state.newTaskDescription} onChange={onDescriptionInputChange}/>
        <button disabled = {buttonDisabled} onClick={onInsertButtonClick}>Insert</button>
        <ul>
          { _.map(this.props.tasks, this.createTaskComponent) }
        </ul>
      </div>
    );
  }
});


// Connect TaskList component to the Redux store. That is, we will use
// parts of the store to pass props to the TaskList component.
const ConnectedTaskList = ReactRedux.connect(
  // Map store state to props
  (state) => ({
    tasks: state.tasks
  }),
  // Map action dispatchers to props
  // NOTE: _.flow(f, g) returns a function equivalent to g(f(args...))
  (dispatch) => ({
    insertTask: _.flow(actionCreators.insertTask, dispatch),
    removeTask: _.flow(actionCreators.removeTask, dispatch)
  })
)(TaskList);

// Export the connected TaskList component
module.exports = ConnectedTaskList;
